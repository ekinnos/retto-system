package = {
    "AUTHOR":"Ekin Varli",
    "LICENSE":"MIT",
    "VERSION":"2.0.0",
    "GIT":"https://github.com/ekinnos/retto-system.git"
}