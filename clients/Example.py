
"""

Retto example client

"""

from pyclbr import Function
import socket
import random
from threading import Thread

from datetime import datetime
import time
from colorama import Fore, init, Back


class Example():

    @Function
    def example():

        init()


        colors = [Fore.BLUE, Fore.CYAN, Fore.GREEN, Fore.LIGHTBLACK_EX, 
            Fore.LIGHTBLUE_EX, Fore.LIGHTCYAN_EX, Fore.LIGHTGREEN_EX, 
            Fore.LIGHTMAGENTA_EX, Fore.LIGHTRED_EX, Fore.LIGHTWHITE_EX, 
            Fore.LIGHTYELLOW_EX, Fore.MAGENTA, Fore.RED, Fore.WHITE, Fore.YELLOW
        ]

        client_color = random.choice(colors)


        print('Example client')

        HOSTNAME = input('Host: ')
        PORT = input('Port: ')


        soket = socket.socket()
        print(f'Connection to {HOSTNAME}:{PORT}...')

        soket.connect((HOSTNAME,PORT))
        print('Connected to server.')