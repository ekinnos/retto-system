
"""

Retto System

by Ekinnos


"""


import socket
import time
import datetime
import colorama
import string
import sqlite3
import os
import sys
import subprocess

from threading import Thread
from random import random as rd
from Login import *
from Register import *
from ast import FunctionDef
from pyclbr import Function


class Server():

    @Function
    def main():

        HOSTNAME = 'localhost'
        PORT = 6698
        
        soket = socket.socket()
        soket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        clientSockets = set()

        soket.bind((HOSTNAME, PORT))
        soket.listen(5)

        
        @Function
        def listenForClient(cs):
            
            while True:

                try:
                    msg = cs.recv(1024).decode()
                except Exception as e:
                    print(f'Error: {e}')

                    clientSockets.remove(cs)
                else:

                    msg = msg.replace(': ')

                
                for clientSocket in clientSockets:
                    
                    clientSocket.send(msg.encode())

        
        while True:

            clientSocket, clientAddress = soket.accept()

            print(f'LOG: {clientAddress} is connected.')

            clientSockets.add(clientSocket)

            
            threadEvent = Thread(target=listenForClient, args=(clientSocket))
            threadEvent.daemon = True

            threadEvent.start()


        for cs in clientSockets:
            cs.close()

        
        soket.close()