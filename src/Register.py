
"""

Register events

"""

import sqlite3
from pyclbr import Function
import time


class Register():

    @Function
    def register():

        connection = sqlite3.connect('users.db')
        cursor = connection.cursor()


        @Function
        def createAccount():

            cursor.execute('''
                CREATE TABLE IF NOT EXISTS users(name TEXT, password TEXT)
            ''')